<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>happy</source>
            <comment>Text</comment>
            <translation type="obsolete">happy</translation>
        </message>
        <message>
            <location filename="../../../../../../../../../../../../../../../../../../../Program Files (x86)/Aldebaran/Choregraphe Suite 2.5/translations/behavior_1/behavior.xar" line="0"/>
            <source></source>
            <comment>Text</comment>
            <translation></translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>surprised</source>
            <comment>Text</comment>
            <translation type="unfinished">surprised</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (2)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>angry</source>
            <comment>Text</comment>
            <translation type="unfinished">angry</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (3)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>sad</source>
            <comment>Text</comment>
            <translation type="unfinished">sad</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (4)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>neutral</source>
            <comment>Text</comment>
            <translation type="unfinished">neutral</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (5)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>smile</source>
            <comment>Text</comment>
            <translation type="obsolete">smile</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>no smile</source>
            <comment>Text</comment>
            <translation type="obsolete">no smile</translation>
        </message>
    </context>
</TS>
